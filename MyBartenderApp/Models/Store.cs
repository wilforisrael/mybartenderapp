﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBartenderApp.Models
{
    public class Store
    {
        public Int64 StoreID { set; get; }
        public string StoreName { set; get; }
        public string Address { set; get; }
        public virtual ICollection<Order> Orders { set; get; }
    }
}