﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBartenderApp.Models
{
    public class DrinkByOrder
    {
        public Int64 DrinkByOrderID { set; get; }
        public Int64 OrderID { set; get; }
        public virtual Order Order { set; get; }
        public Int64 DrinkID { set; get; }
        public virtual Drink Drink { set; get; }
        public decimal Price { set; get; }
        public int Cant { set; get; }
    }
}