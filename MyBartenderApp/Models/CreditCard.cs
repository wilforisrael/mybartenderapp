﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBartenderApp.Models
{
    public class CreditCard
    {
        public Int64 CreditCardID { set; get; }
        public string CreditCardName { set; get; }
        public virtual ICollection<Order> Orders { set; get; } 
    }
}