﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBartenderApp.Models
{
    public class Drink
    {
        public Int64 DrinkID { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public int Existence { set; get; }
        public decimal Price { set; get; }
        public virtual ICollection<DrinkByOrder> DrinkByOrders { set; get; }
    }
}