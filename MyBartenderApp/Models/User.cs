﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyBartenderApp.Models
{
    public class User
    {
        public Int64 UserID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Email { set; get; }
        public virtual ICollection<Order> Orders { set; get; }
    }

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(): base("DefaultConnection")
        {
            base.Configuration.ProxyCreationEnabled = false;
        }
        public System.Data.Entity.DbSet<MyBartenderApp.Models.CreditCard> CreditCards { get; set; }
        public System.Data.Entity.DbSet<MyBartenderApp.Models.Drink> Drinks { get; set; }
        public System.Data.Entity.DbSet<MyBartenderApp.Models.DrinkByOrder> DrinkByOrders { get; set; }
        public System.Data.Entity.DbSet<MyBartenderApp.Models.Order> Orders { get; set; }
        public System.Data.Entity.DbSet<MyBartenderApp.Models.Store> Stores { get; set; }
        public System.Data.Entity.DbSet<MyBartenderApp.Models.User> Users { get; set; }
    }
}