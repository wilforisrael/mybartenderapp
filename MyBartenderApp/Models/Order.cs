﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBartenderApp.Models
{
    public class Order
    {
        public Int64 OrderID { set; get; }
        public DateTime DateOrder { set; get; } 
        public Int64 UserID { set; get; }
        public virtual User User { set; get; }
        public Int64 StoreID { set; get; }
        public virtual Store Store { set; get; }
        public decimal Total { set; get; }
        public int Pay { set; get; }
        public Int64 CreditCardID { set; get; }
        public virtual CreditCard CreditCard { set; get; }
        public string CreditCardNumber { set; get; }
        public string CreditCardNameUser { set; get; }
        public string ExpirationDay { set; get; }
        public int SecretCode { set; get; }
    }
}