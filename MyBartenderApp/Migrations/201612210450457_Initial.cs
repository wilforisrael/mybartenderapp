namespace MyBartenderApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CreditCards",
                c => new
                    {
                        CreditCardID = c.Long(nullable: false, identity: true),
                        CreditCardName = c.String(),
                    })
                .PrimaryKey(t => t.CreditCardID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Long(nullable: false, identity: true),
                        DateOrder = c.DateTime(nullable: false),
                        UserID = c.Long(nullable: false),
                        StoreID = c.Long(nullable: false),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Pay = c.Int(nullable: false),
                        CreditCardID = c.Long(nullable: false),
                        CreditCardNumber = c.String(),
                        CreditCardNameUser = c.String(),
                        ExpirationDay = c.String(),
                        SecretCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.CreditCards", t => t.CreditCardID, cascadeDelete: true)
                .ForeignKey("dbo.Stores", t => t.StoreID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.StoreID)
                .Index(t => t.CreditCardID);
            
            CreateTable(
                "dbo.Stores",
                c => new
                    {
                        StoreID = c.Long(nullable: false, identity: true),
                        StoreName = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.StoreID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.DrinkByOrders",
                c => new
                    {
                        DrinkByOrderID = c.Long(nullable: false, identity: true),
                        OrderID = c.Long(nullable: false),
                        DrinkID = c.Long(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cant = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DrinkByOrderID)
                .ForeignKey("dbo.Drinks", t => t.DrinkID, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true)
                .Index(t => t.OrderID)
                .Index(t => t.DrinkID);
            
            CreateTable(
                "dbo.Drinks",
                c => new
                    {
                        DrinkID = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Existence = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.DrinkID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DrinkByOrders", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.DrinkByOrders", "DrinkID", "dbo.Drinks");
            DropForeignKey("dbo.Orders", "UserID", "dbo.Users");
            DropForeignKey("dbo.Orders", "StoreID", "dbo.Stores");
            DropForeignKey("dbo.Orders", "CreditCardID", "dbo.CreditCards");
            DropIndex("dbo.DrinkByOrders", new[] { "DrinkID" });
            DropIndex("dbo.DrinkByOrders", new[] { "OrderID" });
            DropIndex("dbo.Orders", new[] { "CreditCardID" });
            DropIndex("dbo.Orders", new[] { "StoreID" });
            DropIndex("dbo.Orders", new[] { "UserID" });
            DropTable("dbo.Drinks");
            DropTable("dbo.DrinkByOrders");
            DropTable("dbo.Users");
            DropTable("dbo.Stores");
            DropTable("dbo.Orders");
            DropTable("dbo.CreditCards");
        }
    }
}
